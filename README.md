# amp_coding_challenge

## Congratulations!

You are being selected as one of the best candidates for this job. Before hiring, we would like to see some of your skills in a relatively easy task.

## The task
You can find a default project in this repository. Please create an onlongpress event when pressing on the button, which then fires an animation similar like the one in whatsapp when you want to create a voice message.

Check out this video for a showcase of the animation (0.03 - 0:06)
https://www.youtube.com/watch?v=rAkfhA6Cwlg

After the animation happens, a debugPrint event should be called when the finger where it leaves the display is in the same area as the lock icon. Otherwise, do nothing.

Good luck participants! 

Please send us an email to dev@amptech.store as soon as you are finished with the task. Also feel free to send as an email if you have any kind of question regarding the task.

Kind regards

AMPTech IT Team
